﻿using Caliburn.Micro;
using System.Windows;

namespace Test.Api
{
    /// <summary>
    /// Interaction logic for Client.xaml
    /// </summary>
    public partial class Client : Application, IHandle<Messages.ExitApplicationMessage>
    {
        public Client()
        {
            InitializeComponent();

            this.DispatcherUnhandledException += Client_DispatcherUnhandledException;
            EventAggregatorHandler.Instance.Subscribe(this);
        }

        private void Client_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            EventAggregatorHandler.PublishOnUIThread(new Test.Api.Messages.ExceptionMessage("Client", null, e.Exception));
            e.Handled = true;
        }

        public void Handle(Messages.ExitApplicationMessage message)
        {
            this.Shutdown(message.ExitCode);
        }
    }
}
