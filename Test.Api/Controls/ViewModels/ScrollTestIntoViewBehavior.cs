﻿using System.Windows.Controls;
using System.Windows.Interactivity;

namespace Test.Api.Controls.ViewModels
{
    public class ScrollTestIntoViewBehavior : Behavior<DataGrid>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.LoadingRow += AssociatedObject_LoadingRow;
            this.AssociatedObject.UnloadingRow += AssociatedObject_UnloadingRow;
        }

        private void AssociatedObject_UnloadingRow(object sender, DataGridRowEventArgs e)
        {
            var test = e.Row.Item as TestFile;
            if (test != null)
            {
                test.PropertyChanged -= test_PropertyChanged;
            }
        }

        private void test_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var test = sender as TestFile;
            if (e.PropertyName == "RunState" && test != null && test.RunState == Common.RunState.Running)
            {
                this.AssociatedObject.ScrollIntoView(sender);
            }
        }

        private void AssociatedObject_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var test = e.Row.Item as TestFile;
            if (test != null)
            {
                test.PropertyChanged += test_PropertyChanged;
            }
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.LoadingRow -= AssociatedObject_LoadingRow;
            this.AssociatedObject.UnloadingRow -= AssociatedObject_UnloadingRow;
        }
    }
}
