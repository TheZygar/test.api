﻿using Caliburn.Micro;
using System;
using System.Configuration;
using System.IO;

namespace Test.Api.Controls.ViewModels
{
    public class SettingsViewModel : Screen
    {
        private static SettingsViewModel _instance = null;

        public static SettingsViewModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SettingsViewModel();
                }
                return _instance;
            }
        }

        public static string FromRegistry(string keyName)
        {
            if (!string.IsNullOrEmpty(keyName))
            {
                using (var key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\MyCoreTest"))
                {
                    if (null != key)
                    {
                        return key.GetValue(keyName) as string;
                    }
                }
            }
            return string.Empty;
        }

        public static void ToRegistry(string keyName, string value)
        {
            try
            {
                if (string.IsNullOrEmpty(keyName))
                {
                    return;
                }
                var key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\MyCoreTest", true);
                if (null == key)
                {
                    key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"Software\MyCoreTest");
                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\MyCoreTest", true);
                }
                key.SetValue(keyName, value);
                key.Close();
            }
            catch { }
        }

        private SettingsViewModel()
        {
            TestFileRoot = ConfigurationManager.AppSettings["TestFileRoot"];
            DestinationFileRoot = ConfigurationManager.AppSettings["DestinationFileRoot"];
            Uri = ConfigurationManager.AppSettings["EnvironmentUri"];
            EventAggregatorHandler.Instance.Subscribe(this);
        }

        public string Environment
        {
            get
            {
                return ConfigurationManager.AppSettings["EnvironmentVersion"];
            }
        }

        private string _testFileRoot = null;

        public string TestFileRoot
        {
            get
            {
                return _testFileRoot;
            }
            set
            {
                string rooted = value;
                if (!Path.IsPathRooted(rooted))
                {
                    rooted = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, rooted);
                }
                if (!Directory.Exists(rooted))
                {
                    var info = Directory.CreateDirectory(rooted);
                    rooted = info.FullName;
                }
                else
                {
                    var info = new DirectoryInfo(rooted);
                    rooted = info.FullName;
                }
                _testFileRoot = rooted;
                NotifyOfPropertyChange(() => TestFileRoot);
            }
        }

        private string _destinationFileRoot = null;

        public string DestinationFileRoot
        {
            get
            {
                return _destinationFileRoot;
            }
            set
            {
                string rooted = value;
                if (!Path.IsPathRooted(rooted))
                {
                    rooted = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, rooted);
                }
                if (!Directory.Exists(rooted))
                {
                    var info = Directory.CreateDirectory(rooted);
                    rooted = info.FullName;
                }
                else
                {
                    var info = new DirectoryInfo(rooted);
                    rooted = info.FullName;
                }
                _destinationFileRoot = rooted;
                NotifyOfPropertyChange(() => DestinationFileRoot);
            }
        }

        private string _uri = null;

        public string Uri
        {
            get
            {
                return _uri;
            }
            set
            {
                _uri = value;
                NotifyOfPropertyChange(() => Uri);
            }
        }

        private string _compareTool = FromRegistry("DiffToolPath");

        public string CompareTool
        {
            get
            {
                return _compareTool;
            }
            set
            {
                _compareTool = value;
                ToRegistry("DiffToolPath", _compareTool);
                NotifyOfPropertyChange(() => CompareTool);
            }
        }

        private TimeSpan _responseThreshold = TimeSpan.FromSeconds(5);

        public TimeSpan ResponseThreshold
        {
            get
            {
                return _responseThreshold;
            }
            set
            {
                _responseThreshold = value;
                NotifyOfPropertyChange(() => ResponseThreshold);
            }
        }

        ~SettingsViewModel()
        {
            EventAggregatorHandler.Instance.Unsubscribe(this);
        }
    }
}
