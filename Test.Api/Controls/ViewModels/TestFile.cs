﻿using Caliburn.Micro;
using System;
using System.IO;
using Test.Api.Common;

namespace Test.Api.Controls.ViewModels
{
    public class TestFile : PropertyChangedBase
    {
        public TestFile(string displayName, string testFileName)
        {
            DisplayName = displayName;
            TestFileName = testFileName;
        }

        private string _displayName = null;

        public string DisplayName
        {
            get
            {
                return _displayName;
            }
            set
            {
                _displayName = value;
                NotifyOfPropertyChange(() => DisplayName);
            }
        }

        public string ShortFileName { get; private set; }

        private string _testFileName = null;

        public string TestFileName
        {
            get
            {
                return _testFileName;
            }
            set
            {
                _testFileName = value;
                NotifyOfPropertyChange(() => TestFileName);

                if (string.IsNullOrEmpty(_testFileName))
                {
                    ShortFileName = null;
                }

                string root = SettingsViewModel.Instance.TestFileRoot.ToLowerInvariant();
                if (_testFileName.ToLowerInvariant().StartsWith(root) && _testFileName.Length > root.Length)
                {
                    ShortFileName = _testFileName.Substring(root.Length + 1);
                }
                else
                {
                    ShortFileName = _testFileName;
                }
            }
        }

        private bool _isPaused = false;

        public bool IsPaused
        {
            get
            {
                return _isPaused;
            }
            set
            {
                _isPaused = value;
                NotifyOfPropertyChange(() => IsPaused);
            }
        }

        public string ResultFileName
        {
            get
            {
                return Path.ChangeExtension(_testFileName, "result");
            }
        }

        public bool HasRun
        {
            get
            {
                switch (RunState)
                {
                    case RunState.NotRun:
                    case RunState.Running:
                    case RunState.Paused:
                        return false;

                    default:
                        return true;
                }
            }
            set
            {
            }
        }

        public bool HasResult
        {
            get
            {
                switch (RunState)
                {
                    case RunState.Success:
                    case RunState.CompareFailure:
                        return true;

                    default:
                        return false;
                }
            }
            set
            {
            }
        }

        private RunState _runState = RunState.NotRun;

        public RunState RunState
        {
            get
            {
                return _runState;
            }
            set
            {
                _runState = value;
                NotifyOfPropertyChange(() => RunState);
                NotifyOfPropertyChange(() => HasRun);
                NotifyOfPropertyChange(() => HasResult);
            }
        }

        public static void OpenTest(TestFile context)
        {
            if (System.IO.File.Exists(context.TestFileName))
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = new System.Diagnostics.ProcessStartInfo() { FileName = context.TestFileName };
                process.Start();
            }
        }

        public static void OpenExpectedResult(TestFile context)
        {
            if (System.IO.File.Exists(context.ResultFileName))
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = new System.Diagnostics.ProcessStartInfo() { FileName = context.ResultFileName };
                process.Start();
            }
        }

        public static void OpenCompare(TestFile context)
        {
            string filename = Path.GetFileName(context.ResultFileName);
            filename = Path.Combine(SettingsViewModel.Instance.DestinationFileRoot, filename);

            string compareTool = SettingsViewModel.Instance.CompareTool;
            if (!string.IsNullOrEmpty(compareTool))
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = new System.Diagnostics.ProcessStartInfo()
                {
                    FileName = compareTool,
                    Arguments = String.Format("\"{0}\" \"{1}\"", context.ResultFileName, filename)
                };
                process.Start();
            }
        }

        public static void OpenRaw(TestFile context)
        {
            string filename = Path.GetFileName(context.ResultFileName);
            filename = Path.Combine(SettingsViewModel.Instance.DestinationFileRoot, filename);
            filename = Path.ChangeExtension(filename, "raw");

            if (System.IO.File.Exists(filename))
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = new System.Diagnostics.ProcessStartInfo() { FileName = filename };
                process.Start();
            }
        }
    }
}
