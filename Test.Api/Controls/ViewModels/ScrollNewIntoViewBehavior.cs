﻿using System.Windows.Controls;
using System.Windows.Interactivity;

namespace Test.Api.Controls.ViewModels
{
    public class ScrollNewIntoViewBehavior : Behavior<DataGrid>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.LoadingRow += AssociatedObject_LoadingRow;
        }

        private void AssociatedObject_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            int index = e.Row.GetIndex();
            if (index > 0)
            {
                this.AssociatedObject.ScrollIntoView(this.AssociatedObject.Items[index - 1]);
            }
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.LoadingRow -= AssociatedObject_LoadingRow;
        }
    }
}
