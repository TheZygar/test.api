﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Test.Api.Controls.ViewModels
{
    public class Metric : PropertyChangedBase
    {
        public Metric(string name)
        {
            Name = name;
            Timing = new List<TimeSpan>();
        }

        private List<TimeSpan> _timing = null;

        public List<TimeSpan> Timing
        {
            get
            {
                return _timing;
            }
            set
            {
                _timing = value;
                NotifyOfPropertyChange(() => Timing);
                NotifyOfPropertyChange(() => Average);
                NotifyOfPropertyChange(() => Maximum);
                NotifyOfPropertyChange(() => Minimum);
            }
        }

        public int Count
        {
            get
            {
                if (_timing != null)
                {
                    return _timing.Count();
                }
                return 0;
            }
        }

        public double Average
        {
            get
            {
                if (_timing != null)
                {
                    return _timing.Average(t => t.TotalSeconds);
                }
                return 0;
            }
        }

        public double Maximum
        {
            get
            {
                if (_timing != null)
                {
                    return _timing.Max(t => t.TotalSeconds);
                }
                return 0;
            }
        }

        public double Minimum
        {
            get
            {
                if (_timing != null)
                {
                    return _timing.Min(t => t.TotalSeconds);
                }
                return 0;
            }
        }

        private string _name = null;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }
    }
}
