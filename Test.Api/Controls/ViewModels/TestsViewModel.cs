﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Test.Api.Common;
using Test.Api.Messages;

namespace Test.Api.Controls.ViewModels
{
    public class TestsViewModel : Screen, IHandle<RunTestsMessage>, IHandleWithTask<RunMessage>, IHandle<RequestTimingMessage>, IHandle<ExceptionMessage>, IHandle<StatusMessage>
    {
        private string LogFile = null;

        private static bool? _auto = null;

        public bool Auto
        {
            get
            {
                if (!_auto.HasValue)
                {
                    string auto = System.Configuration.ConfigurationManager.AppSettings["Auto"];
                    bool result;
                    if (bool.TryParse(auto, out result))
                    {
                        _auto = result;
                    }
                    else
                    {
                        _auto = false;
                    }
                }
                return _auto.Value;
            }
        }

        public TestsViewModel()
        {
            IsRunningTests = false;
            LoadTestInfos();
            EventAggregatorHandler.Instance.Subscribe(this);
            LogFile = Path.Combine(Settings.DestinationFileRoot, string.Format(".{0:yyyy-MM-ddTHH-mm}.log", DateTime.Now));

            if (Auto)
            {
                Run();
            }
        }

        public void Run()
        {
            EventAggregatorHandler.PublishOnUIThread(new RunTestsMessage());
        }

        private bool _isRunningTests = false;

        public bool IsRunningTests
        {
            get
            {
                return _isRunningTests;
            }
            private set
            {
                _isRunningTests = value;
                NotifyOfPropertyChange(() => IsRunningTests);
            }
        }

        private SettingsViewModel _settings = null;

        public SettingsViewModel Settings
        {
            get
            {
                if (_settings == null)
                {
                    _settings = SettingsViewModel.Instance;
                }
                return _settings;
            }
            set
            {
                _settings = value;
                NotifyOfPropertyChange(() => Settings);
            }
        }

        private bool _showSpinner = false;

        public bool ShowSpinner
        {
            get
            {
                return _showSpinner;
            }
            set
            {
                _showSpinner = value;
                NotifyOfPropertyChange(() => ShowSpinner);
            }
        }

        private string _spinnerMessage = string.Empty;

        public string SpinnerMessage
        {
            get
            {
                return _spinnerMessage;
            }
            set
            {
                _spinnerMessage = value;
                NotifyOfPropertyChange(() => SpinnerMessage);
            }
        }

        private ObservableCollection<TestFileInfo> _testInfos = null;

        public ObservableCollection<TestFileInfo> TestInfos
        {
            get
            {
                return _testInfos;
            }
            set
            {
                _testInfos = value;
                NotifyOfPropertyChange(() => TestInfos);
            }
        }

        private ObservableCollection<TestFile> _tests = new ObservableCollection<TestFile>();

        public ObservableCollection<TestFile> Tests
        {
            get
            {
                return _tests;
            }
            set
            {
                _tests = value;
                NotifyOfPropertyChange(() => Tests);
            }
        }

        private ObservableCollection<WrappedException> _exceptionMessages = null;

        public ObservableCollection<WrappedException> ExceptionMessages
        {
            get
            {
                if (_exceptionMessages == null)
                {
                    _exceptionMessages = new ObservableCollection<WrappedException>();
                    _exceptionMessages.CollectionChanged += _exceptionMessages_CollectionChanged;
                }

                return _exceptionMessages;
            }
            set
            {
                if (_exceptionMessages != null)
                {
                    _exceptionMessages.CollectionChanged -= _exceptionMessages_CollectionChanged;
                }
                _exceptionMessages = value;
                if (_exceptionMessages != null)
                {
                    _exceptionMessages.CollectionChanged += _exceptionMessages_CollectionChanged;
                }
                NotifyOfPropertyChange(() => ExceptionMessages);
                NotifyOfPropertyChange(() => ExceptionMessageHeader);
            }
        }

        public void _exceptionMessages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            NotifyOfPropertyChange(() => ExceptionMessageHeader);
        }

        public string ExceptionMessageHeader
        {
            get { return string.Format("Exceptions ({0})", ExceptionMessages.Count); }
            set { }
        }

        private TestFile _selectedTest = null;

        public TestFile SelectedTest
        {
            get
            {
                return _selectedTest;
            }
            set
            {
                _selectedTest = value;
                NotifyOfPropertyChange(() => SelectedTest);
            }
        }

        private TestFileInfo _selectedTestInfo = null;

        public TestFileInfo SelectedTestInfo
        {
            get
            {
                return _selectedTestInfo;
            }
            set
            {
                _selectedTestInfo = value;
                NotifyOfPropertyChange(() => SelectedTestInfo);
            }
        }

        private ObservableCollection<Metric> _metrics = new ObservableCollection<Metric>();

        public ObservableCollection<Metric> Metrics
        {
            get
            {
                return _metrics;
            }
            set
            {
                _metrics = value;
                NotifyOfPropertyChange(() => Metrics);
            }
        }

        ~TestsViewModel()
        {
            EventAggregatorHandler.Instance.Unsubscribe(this);
        }

        public void Handle(RunTestsMessage message)
        {
            if (!IsRunningTests)
            {
                IsRunningTests = true;

                Tests.Clear();
                if (message.Tests == null || message.Tests.Count() == 0)
                {
                    foreach (var test in TestFileInfo.GetSelectedTests(TestInfos))
                    {
                        Tests.Add(test);
                    }
                }
                else
                {
                    foreach (var test in message.Tests)
                    {
                        Tests.Add(test);
                    }
                }
                SelectedTest = Tests.FirstOrDefault();
                Metrics.Clear();
                ExceptionMessages.Clear();

                EventAggregatorHandler.PublishOnBackgroundThread(new Messages.RunMessage());
            }
        }

        public async Task Handle(RunMessage message)
        {
            try
            {
                File.WriteAllText(Path.Combine(SettingsViewModel.Instance.DestinationFileRoot, "Results.html"), string.Empty);
                using (var manager = new TestManager(new CommandEngine(), Settings.Uri))
                {
                    //TODO:  If you wish to get accurate response timings, set up the following method so it will wake up your server:
                    //manager.WaitForStartup();

                    foreach (var test in Tests)
                    {
                        await manager.Run(test);
                    }
                }
            }
            finally
            {
                IsRunningTests = false;
                if (Auto)
                {
                    EventAggregatorHandler.PublishOnUIThread(new ExitApplicationMessage(ExceptionMessages.Count));
                }
            }
        }

        public void Handle(RequestTimingMessage message)
        {
            var metric = _metrics.FirstOrDefault(x => x.Name.ToLowerInvariant() == message.Name.ToLowerInvariant());
            if (metric != null)
            {
                metric.Timing.Add(message.Time);
            }
            else
            {
                metric = new Metric(message.Name);
                metric.Timing.Add(message.Time);
                Metrics.Add(metric);
                NotifyOfPropertyChange(() => Metrics);
            }
        }

        public void Handle(ExceptionMessage message)
        {
            Console.Error.WriteLine(message.Exception.ToString());
            File.AppendAllText(LogFile, message.Exception.ToString() + "\r\n", System.Text.Encoding.UTF8);
            ExceptionMessages.Add(message.Exception);
        }

        public void SaveTestInfos()
        {
            List<string> tests = new List<string>();

            foreach (var file in TestInfos)
            {
                if (file.IsSelected)
                {
                    tests.Add(Path.GetFileName(file.Location));
                }
                file.Save();
            }

            var testsFile = Path.Combine(Settings.TestFileRoot, ".tests");
            File.WriteAllLines(testsFile, tests.ToArray());

            foreach (var file in TestInfos)
            {
                file.Save();
            }
        }

        public void CollapseAll()
        {
            foreach (var testInfo in TestInfos)
            {
                testInfo.CollapseAll();
            }
        }

        public void LoadTestInfos()
        {
            var testInfo = new TestFileInfo(Settings.TestFileRoot, false, null, null, TestInfos);
            TestInfos = testInfo.TestInfos;
        }

        public void Handle(StatusMessage message)
        {
            ShowSpinner = message.DisplaySpinner;
            SpinnerMessage = message.Status;
        }
    }
}
