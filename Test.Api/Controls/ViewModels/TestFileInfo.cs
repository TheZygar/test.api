﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace Test.Api.Controls.ViewModels
{
    public class TestFileInfo : PropertyChangedBase
    {
        public TestFileInfo(string location, bool selected, string[] defaultSelected, int? defaultIndex, ObservableCollection<TestFileInfo> owner)
        {
            Location = location;
            IsFile = false;
            _owner = owner;
            if (File.Exists(location) && Path.GetExtension(location) == ".test")
            {
                Name = Path.GetFileName(location);
                IsFile = true;
            }
            else if (Directory.Exists(location))
            {
                Name = Path.GetFileName(location);
                var testFileInfos = new List<TestFileInfo>();

                var testsFile = Path.Combine(location, ".tests");
                string[] selectedTests = new string[0];
                if (File.Exists(testsFile))
                {
                    selectedTests = File.ReadAllLines(testsFile);
                }
                var tests = Directory.GetFileSystemEntries(location, "*.*");
                foreach (var test in tests)
                {
                    if (Path.GetExtension(test) == ".test" || Directory.Exists(test))
                    {
                        bool isSelected = selectedTests.Contains(Path.GetFileName(test));
                        testFileInfos.Add(new TestFileInfo(test, isSelected, defaultSelected, isSelected ? Array.IndexOf(selectedTests, Path.GetFileName(test)) : (int?)null, null));
                    }
                }

                TestInfos = new ObservableCollection<TestFileInfo>(testFileInfos.OrderBy(x => x.DefaultIndex.HasValue ? x.DefaultIndex : int.MaxValue).ThenBy(x => x.Name).ToList());
                foreach (var test in TestInfos)
                {
                    test._owner = TestInfos;
                }
            }
            if (defaultSelected == null)
            {
                IsSelected = selected;
                IsExpanded = selected;
            }
            else if (location.StartsWith(SettingsViewModel.Instance.TestFileRoot) &&
                defaultSelected.Any(x => x == location.Substring(SettingsViewModel.Instance.TestFileRoot.Length)))
            {
                IsSelected = true;
                IsExpanded = true;
            }
            DefaultIndex = defaultIndex;
        }

        public void Save()
        {
            if (!IsFile)
            {
                List<string> tests = new List<string>();

                foreach (var file in TestInfos)
                {
                    if (file.IsSelected)
                    {
                        tests.Add(Path.GetFileName(file.Location));
                    }
                    file.Save();
                }

                var testsFile = Path.Combine(Location, ".tests");
                File.WriteAllLines(testsFile, tests.ToArray());
            }
        }

        public static List<TestFile> GetSelectedTests(IList<TestFileInfo> testFileInfos)
        {
            var tests = new List<TestFile>();
            foreach (var test in testFileInfos)
            {
                if (test.IsSelected)
                {
                    if (test.IsFile)
                    {
                        tests.Add(new TestFile(test.Name, test.Location));
                    }
                    else
                    {
                        tests.AddRange(GetSelectedTests(test.TestInfos));
                    }
                }
            }
            return tests;
        }

        public void CollapseAll()
        {
            if (IsExpanded)
            {
                IsExpanded = false;
            }
            if (TestInfos != null)
            {
                foreach (var test in TestInfos)
                {
                    test.CollapseAll();
                }
            }
        }

        public bool IsFile { get; private set; }

        public string Location { get; private set; }

        private ObservableCollection<TestFileInfo> _owner { get; set; }

        private ObservableCollection<TestFileInfo> _testInfos = null;

        public ObservableCollection<TestFileInfo> TestInfos
        {
            get
            {
                return _testInfos;
            }
            set
            {
                _testInfos = value;
                NotifyOfPropertyChange(() => TestInfos);
            }
        }

        public static void RunTests(TestFileInfo context)
        {
            List<TestFile> tests = new List<TestFile>();
            if (context.IsFile)
            {
                tests.Add(new TestFile(context.Name, context.Location));
            }
            else
            {
                tests.AddRange(GetSelectedTests(context.TestInfos));
            }
            EventAggregatorHandler.PublishOnUIThread(new Messages.RunTestsMessage(tests));
        }

        private string _name = null;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        private int? _defaultIndex = null;

        public int? DefaultIndex
        {
            get
            {
                return _defaultIndex;
            }
            set
            {
                _defaultIndex = value;
                NotifyOfPropertyChange(() => DefaultIndex);
            }
        }

        private bool _isSelected = false;

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                NotifyOfPropertyChange(() => IsSelected);
            }
        }

        private bool _isExpanded = false;

        public bool IsExpanded
        {
            get
            {
                return _isExpanded;
            }
            set
            {
                _isExpanded = value;
                NotifyOfPropertyChange(() => IsExpanded);
            }
        }

        public void MoveDown()
        {
            try
            {
                int index = _owner.IndexOf(this);
                if (index < _owner.Count - 1)
                {
                    _owner.Move(index, index + 1);
                }
            }
            catch { }
        }

        public void MoveUp()
        {
            try
            {
                int index = _owner.IndexOf(this);
                if (index > 0)
                {
                    _owner.Move(index, index - 1);
                }
            }
            catch { }
        }
    }
}
