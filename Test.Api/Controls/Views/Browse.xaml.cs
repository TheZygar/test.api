﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Test.Api.Controls.Views
{
    /// <summary>
    /// Interaction logic for Browse.xaml
    /// </summary>
    public partial class Browse : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty LabelProperty;
        public static readonly DependencyProperty ValueProperty;
        public static readonly DependencyProperty SelectFolderProperty;

        static Browse()
        {
            LabelProperty = DependencyProperty.Register("Label", typeof(string), typeof(Browse),
                                                        new PropertyMetadata(string.Empty, Label_Changed));
            ValueProperty = DependencyProperty.Register("Value", typeof(string), typeof(Browse),
                                                        new PropertyMetadata(string.Empty, Value_Changed));
            SelectFolderProperty = DependencyProperty.Register("SelectFolder", typeof(bool), typeof(Browse),
                                                        new PropertyMetadata(false, SelectFolder_Changed));
        }

        public Browse()
        {
            InitializeComponent();
        }

        public string Label
        {
            get { return GetValue(LabelProperty) as string; }
            set
            {
                SetValue(LabelProperty, value);
                OnPropertyChanged("Label");
            }
        }

        public string Value
        {
            get { return GetValue(ValueProperty) as string; }
            set
            {
                SetValue(ValueProperty, value);
                OnPropertyChanged("Value");
            }
        }

        public bool SelectFolder
        {
            get { return (bool)GetValue(SelectFolderProperty); }
            set
            {
                SetValue(SelectFolderProperty, value);
                OnPropertyChanged("SelectFolder");
            }
        }

        private bool _selectFolder = false;

        public void Clear()
        {
            Value = string.Empty;
        }

        public void UpdateLabel()
        {
            labelBrowse.Content = Label;
        }

        public void UpdateValue()
        {
            TextBoxFile.Text = Value;
        }

        public void UpdateSelectFolder()
        {
            _selectFolder = SelectFolder;
        }

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void buttonFindFile_Click(object sender, RoutedEventArgs e)
        {
            if (!_selectFolder)
            {
                var ofd = new System.Windows.Forms.OpenFileDialog();
                ofd.InitialDirectory = Value;
                var dialogResult = ofd.ShowDialog();

                if (dialogResult.HasFlag(System.Windows.Forms.DialogResult.OK))
                {
                    Value = ofd.FileName;
                }
            }
            else
            {
                var ofd = new System.Windows.Forms.FolderBrowserDialog();
                ofd.SelectedPath = Value;
                var dialogResult = ofd.ShowDialog();

                if (dialogResult.HasFlag(System.Windows.Forms.DialogResult.OK))
                {
                    Value = ofd.SelectedPath;
                }
            }
        }

        private void TextBoxFile_TextChanged(object sender, TextChangedEventArgs e)
        {
            Value = TextBoxFile.Text;
        }

        /// Raised when a property on this object has a new value. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private static void Label_Changed(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as Browse).UpdateLabel();
        }

        private static void Value_Changed(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as Browse).UpdateValue();
        }

        private static void SelectFolder_Changed(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as Browse).UpdateSelectFolder();
        }
    }
}
