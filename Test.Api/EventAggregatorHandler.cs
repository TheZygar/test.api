﻿using Caliburn.Micro;

namespace Test.Api
{
    public static class EventAggregatorHandler
    {
        // Singleton instance of the EventAggregator service.
        private static IEventAggregator _eventAggregator = null;

        public static IEventAggregator Instance
        {
            get
            {
                if (_eventAggregator == null)
                {
                    _eventAggregator = new EventAggregator();
                }
                return _eventAggregator;
            }
        }

        public static void PublishOnBackgroundThread(object message)
        {
            Instance.PublishOnBackgroundThread(message);
        }

        public static void PublishOnUIThread(object message)
        {
            Instance.PublishOnUIThread(message);
        }

        public static void PublishOnCurrentThread(object message)
        {
            Instance.PublishOnCurrentThread(message);
        }
    }
}
