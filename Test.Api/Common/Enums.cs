﻿namespace Test.Api.Common
{
    public enum RunState
    {
        NotRun,
        ParseFailure,
        Running,
        Paused,
        Success,
        Failure,
        CompareFailure,
        NoExpectedResult,
        Error
    }
}
