﻿using System;
using Test.Api.Parser.Execution;

namespace Test.Api.Common
{
    public class CommandEngine : DefaultCommandEngine
    {
        private static bool? _auto = null;

        public bool Auto
        {
            get
            {
                if (!_auto.HasValue)
                {
                    string auto = System.Configuration.ConfigurationManager.AppSettings["Auto"];
                    bool result;
                    if (bool.TryParse(auto, out result))
                    {
                        _auto = result;
                    }
                    else
                    {
                        _auto = false;
                    }
                }
                return _auto.Value;
            }
        }

        [Command]
        public override string ReadFtpString(string url, string user, string password, string type)
        {
            if (Auto)
            {
                throw new Exception("ReadFtpString is not intended for use in automated tests.  Please re-factor the test or remove it from the automated tests.");
            }
            else
            {
                return base.ReadFtpString(url, user, password, type);
            }
        }
    }
}
