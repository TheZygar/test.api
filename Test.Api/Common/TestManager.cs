﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Test.Api.Controls.ViewModels;
using Test.Api.Messages;
using Test.Api.Parser;
using Test.Api.Parser.Execution;
using Test.Api.Parser.Tokens;

namespace Test.Api.Common
{
    public class TestManager : IDisposable
    {
        public ExecutionContext Context { get; private set; }

        public string MachineName
        {
            get
            {
                return System.Environment.MachineName.ToLowerInvariant();
            }
        }

        public TestManager(ICommandEngine commandEngine, string Uri)
        {
            Context = new ExecutionContext(commandEngine);

            Context.TestFileRoot = SettingsViewModel.Instance.TestFileRoot;

            Context.Variables.Add("uri", Uri);
            Context.Variables.Add("Today", DateTime.Today.ToString("yyyy-MM-dd"));
            Context.Variables.Add("machineName", MachineName);
        }

        public void Pause(TestFile test)
        {
            test.IsPaused = true;
            test.RunState = RunState.Paused;
            while (test.IsPaused)
            {
                System.Threading.Thread.Sleep(1000);
            }
            test.RunState = RunState.Running;
        }

        public Task Run(TestFile test)
        {
            return Task.Factory.StartNew(() =>
            {
                test.RunState = RunState.Running;
                try
                {
                    Context.CurrentTestLocation = Path.GetDirectoryName(test.TestFileName);
                    if (test.IsPaused)
                    {
                        Pause(test);
                    }

                    var parser = new ParseManager();
                    var result = new List<Parser.Tokens.Token>();
                    using (var raw = new StreamWriter(Path.Combine(SettingsViewModel.Instance.DestinationFileRoot, Path.ChangeExtension(Path.GetFileName(test.ResultFileName), "raw")), false, Encoding.UTF8))
                    {
                        try
                        {
                            foreach (var token in parser.ParseFile(test.TestFileName))
                            {
                                //TODO: Improve the parser's ability to detect parser failures
                                if (parser.Exceptions.Count > 0)
                                {
                                    test.RunState = RunState.ParseFailure;
                                    foreach (var ex in parser.Exceptions)
                                    {
                                        EventAggregatorHandler.PublishOnUIThread(new ExceptionMessage(test.ShortFileName, test.ShortFileName, ex));
                                    }
                                    return;
                                }
                                token.WriteTo(raw, null);
                                result.Add(token);

                                var directiveToken = token as DirectiveToken;
                                if (directiveToken != null)
                                {
                                    switch (directiveToken.Command.ToLowerInvariant())
                                    {
                                        case "pause":
                                            if (directiveToken.Parameter == null || directiveToken.Parameter.Value.ToLowerInvariant() == MachineName)
                                            {
                                                Pause(test);
                                            }
                                            break;
                                    }
                                }

                                var stopWatch = new System.Diagnostics.Stopwatch();
                                stopWatch.Start();
                                var response = token.Evaluate(Context);
                                stopWatch.Stop();
                                var time = stopWatch.Elapsed;
                                var responseToken = response as ResponseToken;

                                if (responseToken != null)
                                {
                                    responseToken.WriteTo(raw, null);
                                    foreach (var key in responseToken.Response.Keys)
                                    {
                                        Context.AddOrUpdate(key, responseToken.Response[key]);
                                    }
                                    result.Add(responseToken);
                                    if (responseToken.Response.ContainsKey("Error"))
                                    {
                                        EventAggregatorHandler.PublishOnUIThread(new ExceptionMessage(test.ShortFileName, test.ShortFileName,
                                            new Parser.Exceptions.ResponseException(string.Format("Received Server Error: {0}", responseToken.Response["Error"]))));
                                    }
                                }
                                else if (response != null)
                                {
                                    raw.WriteLine(response);
                                }
                                var requestToken = token as RequestToken;
                                if (requestToken != null)
                                {
                                    EventAggregatorHandler.PublishOnUIThread(new RequestTimingMessage(requestToken.ServiceMethod, time));
                                }
                            }
                        }
                        catch (Parser.Exceptions.EvaluationException ex)
                        {
                            test.RunState = RunState.Failure;
                            EventAggregatorHandler.PublishOnUIThread(new ExceptionMessage(test.ShortFileName, test.ShortFileName, ex));
                            return;
                        }
                        catch (Parser.Exceptions.ParserException ex)
                        {
                            test.RunState = RunState.ParseFailure;
                            EventAggregatorHandler.PublishOnUIThread(new ExceptionMessage(test.ShortFileName, test.ShortFileName, ex));
                            return;
                        }
                        catch (Parser.Exceptions.ParserFileException ex)
                        {
                            test.RunState = RunState.ParseFailure;
                            EventAggregatorHandler.PublishOnUIThread(new ExceptionMessage(test.ShortFileName, test.ShortFileName, ex));
                            return;
                        }
                        catch (EndOfStreamException ex)
                        {
                            test.RunState = RunState.ParseFailure;
                            EventAggregatorHandler.PublishOnUIThread(new ExceptionMessage(test.ShortFileName, test.ShortFileName, ex));
                            return;
                        }
                    }

                    if (CheckResult(test.ResultFileName, result.ToArray()))
                    {
                        test.RunState = RunState.Success;
                    }
                    else
                    {
                        if (File.Exists(test.ResultFileName))
                        {
                            test.RunState = RunState.CompareFailure;
                            File.AppendAllLines(Path.Combine(SettingsViewModel.Instance.DestinationFileRoot, "Results.html"), new string[]
                            {
                                "<p>",
                                string.Format("<a href=\"{1}/{0}\">{0}</>", Path.GetFileName(test.ResultFileName), SettingsViewModel.Instance.Environment),
                                string.Format("<a href=\"{1}/{0}\">{0}</>", Path.ChangeExtension(Path.GetFileName(test.ResultFileName), "raw"), SettingsViewModel.Instance.Environment),
                                "</p>"
                            });
                            EventAggregatorHandler.PublishOnUIThread(new ExceptionMessage(test.ShortFileName, test.ShortFileName,
                                new Exception(string.Format("Compare failure on file {0}", test.DisplayName))));
                        }
                        else
                        {
                            test.RunState = RunState.NoExpectedResult;
                        }
                    }
                }
                catch (Exception ex)
                {
                    test.RunState = RunState.Error;
                    EventAggregatorHandler.PublishOnUIThread(new ExceptionMessage(test.ShortFileName, test.ShortFileName, ex));
                }
            });
        }

        public bool CheckResult(string expected, Token[] actual)
        {
            string filename = Path.GetFileName(expected);
            filename = Path.Combine(SettingsViewModel.Instance.DestinationFileRoot, filename);

            TokenManager tokenManager = new TokenManager();
            var result = tokenManager.Save(filename, actual, Context.Masks.ToArray());

            var hashManager = new Crc32Hash();
            string expectedHash = hashManager.HashFile(expected);
            string actualHash = hashManager.HashFile(result);

            return expectedHash == actualHash;
        }

        internal void WaitForStartup()
        {
            try
            {
                EventAggregatorHandler.PublishOnUIThread(new StatusMessage(true, "Waiting for the environment to become responsive"));

                Context.CommandEngine.Execute(Context, "WaitForResult", new object[] {
                    Context.Variables["uri"],
                    "/Api/Admin.svc/Ping",
                    "{\"Success\":true}",
                    new System.Numerics.BigInteger(TimeSpan.FromMinutes(5).TotalMilliseconds)
                });
            }
            catch (Exception ex)
            {
                EventAggregatorHandler.PublishOnUIThread(new ExceptionMessage("WaitForStartup", null, ex));
            }
            finally
            {
                EventAggregatorHandler.PublishOnUIThread(new StatusMessage(false));
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Context != null)
                {
                    Context.Dispose();
                    Context = null;
                }
            }
        }

        ~TestManager()
        {
            Dispose(false);
        }
    }
}
