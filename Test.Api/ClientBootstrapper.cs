﻿using Caliburn.Micro;
using System.Windows;

namespace Test.Api
{
    public class ClientBootstrapper : BootstrapperBase
    {
        public ClientBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<Test.Api.Screens.ViewModels.HomeViewModel>();
        }

        protected override void OnUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            EventAggregatorHandler.Instance.PublishOnBackgroundThread(new Test.Api.Messages.ExceptionMessage("Root", null, e.Exception));
            e.Handled = true;
        }
    }
}
