﻿using System;
using System.Windows.Data;

namespace Test.Api.Converters
{
    public class TinyTimeSpanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var myValue = (TimeSpan)value;

            return string.Format("{0}.{1:000}", (long)myValue.TotalSeconds, myValue.Milliseconds);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double myValue;
            if (double.TryParse(value.ToString(), out myValue))
            {
                return TimeSpan.FromSeconds(myValue);
            }
            else
            {
                return TimeSpan.FromSeconds(5);
            }
        }
    }
}
