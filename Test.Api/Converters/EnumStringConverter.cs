﻿using System;
using System.Text;
using System.Windows.Data;

namespace Test.Api.Converters
{
    public class EnumStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string s = value.ToString();
            StringBuilder result = new StringBuilder();
            bool first = true;
            foreach (var c in s)
            {
                if (char.IsUpper(c) && !first)
                {
                    result.Append(" ");
                }
                result.Append(c);
                first = false;
            }
            return result.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
