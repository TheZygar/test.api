﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Test.Api.Converters
{
    public class FalseIsVisibleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool myValue = (bool)value;

            Visibility visibility = Visibility.Visible;
            if (myValue)
            {
                visibility = Visibility.Collapsed;
            }
            return visibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
