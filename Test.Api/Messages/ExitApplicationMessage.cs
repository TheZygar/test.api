﻿namespace Test.Api.Messages
{
    public class ExitApplicationMessage
    {
        public ExitApplicationMessage(int exitCode = 0)
        {
            ExitCode = exitCode;
        }

        public int ExitCode { get; private set; }
    }
}
