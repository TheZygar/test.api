﻿using System;

namespace Test.Api.Messages
{
    public class RequestTimingMessage
    {
        public RequestTimingMessage(string name, TimeSpan time)
        {
            Name = name;
            Time = time;
        }

        public string Name { get; private set; }

        public TimeSpan Time { get; private set; }
    }
}
