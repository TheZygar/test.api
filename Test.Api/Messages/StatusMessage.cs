﻿namespace Test.Api.Messages
{
    public class StatusMessage
    {
        public StatusMessage(bool displaySpinner, string status)
        {
            DisplaySpinner = displaySpinner;
            Status = status;
        }

        public StatusMessage(bool displaySpinner)
            : this(displaySpinner, string.Empty)
        {
        }

        public string Status { get; private set; }

        public bool DisplaySpinner { get; private set; }
    }
}
