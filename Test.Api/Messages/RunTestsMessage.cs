﻿using System.Collections.Generic;
using Test.Api.Controls.ViewModels;

namespace Test.Api.Messages
{
    public class RunTestsMessage
    {
        public RunTestsMessage()
        {
        }

        public RunTestsMessage(IList<TestFile> tests)
        {
            Tests = tests;
        }

        public IList<TestFile> Tests { get; set; }
    }
}
