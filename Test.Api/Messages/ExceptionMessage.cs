﻿using Caliburn.Micro;
using System;

namespace Test.Api.Messages
{
    public class ExceptionMessage
    {
        public ExceptionMessage(string origin, string fileName, Exception exception)
        {
            Exception = new WrappedException(origin, fileName, exception);
        }

        public WrappedException Exception { get; private set; }
    }

    public class WrappedException : PropertyChangedBase
    {
        public DateTime Timestamp { get; private set; }

        public Exception InnerException { get; private set; }

        public string Message { get; private set; }

        public string Origin { get; private set; }
        public string FileName { get; private set; }

        public WrappedException(string origin, string fileName, Exception exception)
        {
            Timestamp = DateTime.Now;
            Message = exception.Message;
            FileName = fileName;
            Origin = origin;
            InnerException = exception;
        }

        public override string ToString()
        {
            return string.Format("{0:yyyy-MM-ddTHH:mm:ss.fff} : {1} at {2} \r\n{3}", Timestamp, Message, Origin, InnerException.ToString());
        }
    }
}
