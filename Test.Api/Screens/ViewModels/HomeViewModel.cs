﻿using Caliburn.Micro;
using System.Configuration;
using Test.Api.Controls.ViewModels;

namespace Test.Api.Screens.ViewModels
{
    public class HomeViewModel : Screen
    {
        public HomeViewModel()
        {
            EventAggregatorHandler.Instance.Subscribe(this);
        }

        public string Title
        {
            get
            {
                return Environment + " API Test";
            }
        }

        public string Version
        {
            get
            {
                return "Version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string Environment
        {
            get
            {
                return ConfigurationManager.AppSettings["EnvironmentVersion"];
            }
        }

        private TestsViewModel _tests = null;

        public TestsViewModel Tests
        {
            get
            {
                if (_tests == null)
                {
                    _tests = new TestsViewModel();
                }
                return _tests;
            }
            set
            {
                _tests = value;
                NotifyOfPropertyChange(() => Tests);
            }
        }

        ~HomeViewModel()
        {
            EventAggregatorHandler.Instance.Unsubscribe(this);
        }
    }
}
