﻿using Caliburn.Micro;

namespace Test.Api
{
    public static class WindowManagerFactory
    {
        // Singleton instance of the WindowManager service.
        private static IWindowManager _windowManager = null;

        public static IWindowManager Instance
        {
            get
            {
                if (_windowManager == null)
                {
                    _windowManager = new WindowManager();
                }
                return _windowManager;
            }
        }
    }
}
