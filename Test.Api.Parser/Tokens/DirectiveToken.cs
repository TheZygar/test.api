﻿using System.Linq;
using System.Text;

namespace Test.Api.Parser.Tokens
{
    public class DirectiveToken : Token
    {
        public DirectiveToken(string command, StringToken parameter)
        {
            Command = command;
            Parameter = parameter;
        }

        public string Command { get; private set; }

        public StringToken Parameter { get; private set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder(Command);
            builder.Append(MethodStartIdentifier);

            if (Parameter != null)
            {
                builder.Append(Parameter.ToString());
            }

            builder.Append(MethodEndIdentifier);
            return builder.ToString();
        }

        public override object Evaluate(Execution.ExecutionContext context)
        {
            return null;
        }

        public static bool TryParse(string statement, out DirectiveToken token)
        {
            statement = statement.Trim().TrimEnd(EndOfStatement).Trim();

            if (string.IsNullOrEmpty(statement))
            {
                token = null;
                return false;
            }

            if (statement.First() == DirectiveIdentifier)
            {
                var parts = statement.Split(new char[] { MethodStartIdentifier }, 2);
                if (parts.Length == 2)
                {
                    string command = parts[0].Trim();
                    string parameterString = parts[1].Trim();
                    if (!string.IsNullOrEmpty(parameterString) && parameterString.Last() == MethodEndIdentifier)
                    {
                        if (IsSimpleIdentifier(command))
                        {
                            parameterString = parameterString.TrimEnd(MethodEndIdentifier);
                            if (!string.IsNullOrEmpty(parameterString))
                            {
                                if (parameterString.First() == StringIdentifier)
                                {
                                    StringToken t;
                                    if (StringToken.TryParse(parameterString, out t))
                                    {
                                        token = new DirectiveToken(command, t);
                                        return true;
                                    }
                                }
                            }
                        }
                        token = new DirectiveToken(command, null);
                        return true;
                    }
                }
            }

            token = null;
            return false;
        }
    }
}
