﻿using System.Linq;

namespace Test.Api.Parser.Tokens
{
    public class AssignmentToken : Token
    {
        public AssignmentToken(string assignee, ValuedToken assignment)
        {
            Assignee = assignee;
            Assignment = assignment;
        }

        public string Assignee { get; private set; }

        public ValuedToken Assignment { get; private set; }

        public override string ToString()
        {
            return Assignee.ToString() + " = " + Assignment.ToString();
        }

        public override object Evaluate(Execution.ExecutionContext context)
        {
            var result = Assignment.Evaluate(context);
            context.AddOrUpdate(Assignee, result);

            return result;
        }

        public static bool TryParse(string statement, out AssignmentToken token)
        {
            statement = statement.Trim().TrimEnd(EndOfStatement).Trim();

            string[] statements = statement.Split(new char[] { AssignmentIdentifier }, 2);

            if (statements.Length == 2)
            {
                string assignee = statements[0].Trim();
                if (IsSimpleIdentifier(assignee))
                {
                    string assignmentString = statements[1].Trim();
                    if (!string.IsNullOrEmpty(assignmentString))
                    {
                        if (assignmentString.First() == StringIdentifier && assignmentString.Last() == StringIdentifier)
                        {
                            StringToken t;
                            if (StringToken.TryParse(assignmentString, out t))
                            {
                                token = new AssignmentToken(assignee, t);
                                return true;
                            }
                        }
                        else if (char.IsDigit(assignmentString, 0) || assignmentString.First() == '.' || assignmentString.First() == '-')
                        {
                            NumericToken t;
                            if (NumericToken.TryParse(assignmentString, out t))
                            {
                                token = new AssignmentToken(assignee, t);
                                return true;
                            }
                        }
                        else if (assignmentString[0] == VariableStartIdentifier)
                        {
                            VariableToken t;
                            if (VariableToken.TryParse(assignmentString, out t))
                            {
                                token = new AssignmentToken(assignee, t);
                                return true;
                            }
                        }
                        else if (assignmentString.ToLowerInvariant() == bool.FalseString.ToLowerInvariant() || assignmentString.ToLowerInvariant() == bool.TrueString.ToLowerInvariant())
                        {
                            BooleanToken t;
                            if (BooleanToken.TryParse(assignmentString, out t))
                            {
                                token = new AssignmentToken(assignee, t);
                                return true;
                            }
                        }
                        else
                        {
                            MethodToken t;
                            if (MethodToken.TryParse(assignmentString, out t))
                            {
                                token = new AssignmentToken(assignee, t);
                                return true;
                            }
                        }
                    }
                }
            }

            token = null;
            return false;
        }
    }
}
