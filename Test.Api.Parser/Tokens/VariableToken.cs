﻿using System;
using System.Linq;

namespace Test.Api.Parser.Tokens
{
    public class VariableToken : ValuedToken, IAssignable
    {
        public VariableToken(string accessor)
        {
            Accessor = accessor;
        }

        public string Accessor { get; private set; }

        public override string ToString()
        {
            return VariableStartIdentifier + Accessor + VariableEndIdentifier;
        }

        public override object Evaluate(Execution.ExecutionContext context)
        {
            if (IsSimpleIdentifier(Accessor) && context.Variables.ContainsKey(Accessor))
            {
                return context.Variables[Accessor];
            }
            else
            {
                return context.DictionaryLookup(Accessor, context.Variables);
            }
        }

        public static bool TryParse(string statement, out VariableToken token)
        {
            statement = statement.Trim().TrimEnd(EndOfStatement).Trim();

            if (statement.Length >= 2 && statement.First() == VariableStartIdentifier && statement.Last() == VariableEndIdentifier)
            {
                statement = statement.TrimStart(VariableStartIdentifier).TrimEnd(VariableEndIdentifier);
                if (IsSimpleIdentifier(statement) || IsDictionaryLookup(statement))
                {
                    token = new VariableToken(statement);
                    return true;
                }
            }

            token = null;
            return false;
        }

        public override bool CanGetValue
        {
            get
            {
                return false;
            }
        }

        public override object TokenValue
        {
            get
            {
                throw new NotSupportedException();
            }
        }
    }
}
