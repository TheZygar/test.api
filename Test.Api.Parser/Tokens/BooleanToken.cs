﻿namespace Test.Api.Parser.Tokens
{
    public class BooleanToken : ValuedToken
    {
        public BooleanToken(bool value)
        {
            Value = value;
        }

        public bool Value { get; private set; }

        public override string ToString()
        {
            return Value.ToString();
        }

        public static bool TryParse(string statement, out BooleanToken token)
        {
            var booleanString = statement.Trim().TrimEnd(EndOfStatement).Trim();

            bool value;
            if (bool.TryParse(booleanString, out value))
            {
                token = new BooleanToken(value);
                return true;
            }

            token = null;
            return false;
        }

        public override bool CanGetValue
        {
            get
            {
                return true;
            }
        }

        public override object TokenValue
        {
            get
            {
                return Value;
            }
        }
    }
}
