﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Test.Api.Parser.Exceptions;

namespace Test.Api.Parser.Tokens
{
    public class MethodToken : ValuedToken
    {
        public MethodToken(string command, ValuedToken[] parameters)
        {
            Command = command;
            Parameters = parameters;
        }

        public string Command { get; private set; }

        public ValuedToken[] Parameters { get; private set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder(Command);
            builder.Append(MethodStartIdentifier);

            int count = 1;
            foreach (var parameter in Parameters)
            {
                builder.Append(parameter.ToString());
                if (count < Parameters.Count())
                {
                    builder.Append(Comma);
                }
                count++;
            }

            builder.Append(MethodEndIdentifier);
            return builder.ToString();
        }

        public override object Evaluate(Execution.ExecutionContext context)
        {
            if (!context.CommandEngine.GetAvailableMethods().Contains(Command))
            {
                throw new ParserException("Unable to find the command with the name " + Command);
            }

            var parameters = new List<object>();
            foreach (var parameter in Parameters)
            {
                parameters.Add(parameter.Evaluate(context));
            }

            return context.CommandEngine.Execute(context, Command, parameters.ToArray());
        }

        public static bool TryParse(string statement, out MethodToken token)
        {
            statement = statement.Trim().TrimEnd(EndOfStatement).Trim();

            if (string.IsNullOrEmpty(statement))
            {
                token = null;
                return false;
            }

            var parts = statement.Split(new char[] { MethodStartIdentifier }, 2);
            if (parts.Length == 2)
            {
                string command = parts[0].Trim();
                string parameterString = parts[1].Trim();
                if (!string.IsNullOrEmpty(parameterString) && parameterString.Last() == MethodEndIdentifier)
                {
                    if (IsSimpleIdentifier(command))
                    {
                        parameterString = parameterString.TrimEnd(MethodEndIdentifier);
                        List<ValuedToken> parameters = new List<ValuedToken>();
                        foreach (var p in SplitCommaDelimited(parameterString))
                        {
                            string parameter = p.Trim();
                            if (string.IsNullOrEmpty(parameter))
                            {
                                if (string.IsNullOrEmpty(parameterString))
                                {
                                    continue;
                                }
                                throw new ParserException(string.Format("Parameters of Command: {0}", statement));
                            }

                            if (parameter.First() == StringIdentifier)
                            {
                                StringToken t;
                                if (StringToken.TryParse(parameter, out t))
                                {
                                    parameters.Add(t);
                                    continue;
                                }
                            }
                            else if (char.IsDigit(parameter, 0) || parameter.First() == '.' || parameter.First() == '-')
                            {
                                NumericToken t;
                                if (NumericToken.TryParse(parameter, out t))
                                {
                                    parameters.Add(t);
                                    continue;
                                }
                            }
                            else if (parameter.First() == VariableStartIdentifier)
                            {
                                VariableToken t;
                                if (VariableToken.TryParse(parameter, out t))
                                {
                                    parameters.Add(t);
                                    continue;
                                }
                            }
                            else if (parameter.ToLowerInvariant() == bool.FalseString.ToLowerInvariant() || parameter.ToLowerInvariant() == bool.TrueString.ToLowerInvariant())
                            {
                                BooleanToken t;
                                if (BooleanToken.TryParse(parameter, out t))
                                {
                                    parameters.Add(t);
                                    continue;
                                }
                            }
                            throw new ParserException(string.Format("Parameters of Command: {0}", statement));
                        }
                        token = new MethodToken(command, parameters.ToArray());
                        return true;
                    }
                }
            }

            token = null;
            return false;
        }

        public override bool CanGetValue
        {
            get
            {
                return false;
            }
        }

        public override object TokenValue
        {
            get
            {
                throw new NotSupportedException();
            }
        }
    }
}
