﻿using System.Linq;

namespace Test.Api.Parser.Tokens
{
    public class StringToken : ValuedToken
    {
        public StringToken(string value)
        {
            Value = value;
        }

        public string Value { get; private set; }

        public override string ToString()
        {
            return StringIdentifier + Value.Replace("\"", "\\\"") + StringIdentifier;
        }

        public static bool TryParse(string statement, out StringToken token)
        {
            var value = statement.Trim().TrimEnd(EndOfStatement).Trim();

            if (value.Length >= 2 && value.First() == StringIdentifier && value.Last() == StringIdentifier)
            {
                if (value.Count(x => x == StringIdentifier) == 2)
                {
                    token = new StringToken(value.Trim(StringIdentifier));
                    return true;
                }
                else
                {
                    // Allow escaped double quotes in the string
                    bool isInQuote = false;
                    char? previousCharacter = null;
                    foreach (var character in value)
                    {
                        if (character == Token.StringIdentifier)
                        {
                            if (previousCharacter != Token.StringEscapeIdentifier)
                            {
                                isInQuote = !isInQuote;
                            }
                        }
                        previousCharacter = character;
                    }
                    if (!isInQuote)
                    {
                        token = new StringToken(value.Trim(StringIdentifier).Replace("\\\"", "\""));
                        return true;
                    }
                }
            }

            token = null;
            return false;
        }

        public override bool CanGetValue
        {
            get
            {
                return true;
            }
        }

        public override object TokenValue
        {
            get
            {
                return Value;
            }
        }
    }
}
