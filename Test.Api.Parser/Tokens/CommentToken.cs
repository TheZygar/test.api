﻿namespace Test.Api.Parser.Tokens
{
    public class CommentToken : Token
    {
        public CommentToken(string value)
        {
            Value = value;
        }

        public string Value { get; private set; }

        public override string ToString()
        {
            return CommentIdentifier + Value + EndOfLineForWrite;
        }

        public override object Evaluate(Execution.ExecutionContext context)
        {
            return null;
        }

        public static bool TryParse(string statement, out CommentToken token)
        {
            statement = statement.Trim();

            if (statement.StartsWith(CommentIdentifier))
            {
                token = new CommentToken(statement.TrimStart('/'));
                return true;
            }

            token = null;
            return false;
        }
    }
}
