﻿using System.Numerics;

namespace Test.Api.Parser.Tokens
{
    public class NumericToken : ValuedToken
    {
        public BigInteger? IntegerValue { get; private set; }

        public decimal? DecimalValue { get; private set; }

        public NumericToken(BigInteger value)
        {
            IntegerValue = value;
        }

        public NumericToken(decimal value)
        {
            DecimalValue = value;
        }

        public override string ToString()
        {
            return TokenValue.ToString();
        }

        public static bool TryParse(string statement, out NumericToken token)
        {
            var numberString = statement.Trim().TrimEnd(EndOfStatement).Trim();

            BigInteger integerValue;
            decimal decimalValue;
            if (BigInteger.TryParse(numberString, out integerValue))
            {
                token = new NumericToken(integerValue);
                return true;
            }
            else if (decimal.TryParse(numberString, out decimalValue))
            {
                token = new NumericToken(decimalValue);
                return true;
            }

            token = null;
            return false;
        }

        public override bool CanGetValue
        {
            get
            {
                return true;
            }
        }

        public override object TokenValue
        {
            get
            {
                if (IntegerValue.HasValue)
                {
                    return IntegerValue.Value;
                }
                else
                {
                    return DecimalValue.Value;
                }
            }
        }
    }
}
