﻿namespace Test.Api.Parser.Tokens
{
    public abstract class ValuedToken : Token
    {
        public object GetValue()
        {
            if (CanGetValue)
            {
                return TokenValue;
            }
            else
            {
                return null;
            }
        }

        public override object Evaluate(Execution.ExecutionContext context)
        {
            if (CanGetValue)
            {
                return TokenValue;
            }
            return null;
        }

        public abstract bool CanGetValue { get; }

        public abstract object TokenValue { get; }
    }
}
