﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test.Api.Parser.Tokens
{
    public class RequestToken : Token
    {
        public RequestToken(Method requestMethod, VariableToken uri, string serviceMethod, IDictionary<string, object> parameters)
        {
            RequestMethod = requestMethod;
            Uri = uri;
            ServiceMethod = serviceMethod;
            Parameters = parameters;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            if (RequestMethod == Method.Get)
            {
                builder.Append(RequestGetIdentifier).Append(Uri).Append(ServiceMethod);
            }
            else
            {
                builder.Append(RequestPostIdentifier).Append(Uri).Append(ServiceMethod);
            }

            if (Parameters.Count > 0)
            {
                builder.Append(RequestQueryIdentifier);
                int count = 1;

                Dictionary<string, string> parameters = Parameters.ToDictionary(x => x.Key, x => GetParametersString(x.Value));
                foreach (var parameter in parameters)
                {
                    builder.Append(parameter.Key).Append(AssignmentIdentifier).Append(parameter.Value);
                    if (count < Parameters.Count())
                    {
                        builder.Append(RequestQueryAlsoIdentifier);
                    }
                    count++;
                }
            }

            return builder.ToString();
        }

        public override object Evaluate(Execution.ExecutionContext context)
        {
            var response = Encoding.UTF8.GetString(ExecuteWebRequest(context));
            ResponseToken t;
            if (ResponseToken.TryParse(ResponseIdentifier + response, out t))
            {
                return t;
            }
            return response;
        }

        private byte[] ExecuteWebRequest(Execution.ExecutionContext context)
        {
            string uri = Uri.Evaluate(context).ToString();
            var webClient = context.WebClient;

            Dictionary<string, string> parameters = Parameters.ToDictionary(x => x.Key, x => EvaluateParameters(x.Value, context));

            if (RequestMethod == Method.Post)
            {
                string p = string.Join("&", parameters.Values.ToArray());
                return Encoding.UTF8.GetBytes(webClient.UploadString(uri + ServiceMethod, "POST", p));
            }
            else
            {
                StringBuilder url = new StringBuilder(uri);
                url.Append(ServiceMethod);
                if (parameters.Count > 0)
                {
                    url.Append(RequestQueryIdentifier);
                    foreach (var p in parameters)
                    {
                        url.Append(p.Key).Append(AssignmentIdentifier).Append(p.Value).Append(RequestQueryAlsoIdentifier);
                    }
                    url.Remove(url.Length - 1, 1);
                }
                return webClient.DownloadData(url.ToString());
            }
        }

        public Method RequestMethod { get; private set; }

        public VariableToken Uri { get; private set; }

        public string ServiceMethod { get; private set; }

        public IDictionary<string, object> Parameters { get; private set; }

        public static bool TryParse(string statement, out RequestToken token)
        {
            statement = statement.Trim().TrimEnd(EndOfStatement).Trim();

            Method method;

            if (statement.StartsWith(RequestGetIdentifier))
            {
                method = Method.Get;
                statement = statement.Substring(RequestGetIdentifier.Length);
            }
            else if (statement.StartsWith(RequestPostIdentifier))
            {
                method = Method.Post;
                statement = statement.Substring(RequestPostIdentifier.Length);
            }
            else
            {
                token = null;
                return false;
            }

            VariableToken uriToken;
            string variable = statement.Substring(0, statement.IndexOf(VariableEndIdentifier) + 1);
            VariableToken.TryParse(variable, out uriToken);
            string serviceMethod = statement.Substring(variable.Length);
            var parameters = new Dictionary<string, object>();
            if (statement.Contains(RequestQueryIdentifier))
            {
                try
                {
                    var queryParameters = serviceMethod.Substring(serviceMethod.IndexOf(RequestQueryIdentifier) + 1).Split(new char[] { RequestQueryAlsoIdentifier }, StringSplitOptions.RemoveEmptyEntries);
                    serviceMethod = serviceMethod.Substring(0, serviceMethod.IndexOf(RequestQueryIdentifier));
                    foreach (var parameter in queryParameters)
                    {
                        var q = parameter.Split(new char[] { AssignmentIdentifier }, 2);
                        if (q.Length == 2)
                        {
                            string key = q[0].Trim();
                            string p = q[1].Trim();
                            VariableToken t;
                            if (VariableToken.TryParse(p, out t))
                            {
                                parameters.Add(key, t);
                            }
                            else if (p.First() == '{')
                            {
                                parameters.Add(key, ParseVariables(GetDictionary(p)));
                            }
                            else
                            {
                                parameters.Add(key, p);
                            }
                        }
                        else
                        {
                            parameters.Add(parameter, null);
                        }
                    }
                }
                catch //(Exception ex)
                {
                    token = null;
                    return false;
                }
            }

            token = new RequestToken(method, uriToken, serviceMethod, parameters);
            return true;
        }

        private string GetParametersString(object o)
        {
            if (o == null)
            {
                return null;
            }

            var dictionary = o as IDictionary<string, object>;
            if (dictionary != null)
            {
                return GetJsonString(HideVariables(dictionary));
            }

            return o.ToString();
        }

        private string EvaluateParameters(object o, Execution.ExecutionContext context)
        {
            if (o == null)
            {
                return null;
            }

            var dictionary = o as IDictionary<string, object>;
            if (dictionary != null)
            {
                return GetJsonString(EvaluateVariables(dictionary, context));
            }

            return o.ToString();
        }

        private static IDictionary<string, object> ParseVariables(IDictionary<string, object> dictionary)
        {
            var result = new Dictionary<string, object>();
            if (dictionary != null)
            {
                foreach (var key in dictionary.Keys)
                {
                    var s = dictionary[key] as string;
                    if (s != null)
                    {
                        VariableToken vt;
                        if (VariableToken.TryParse(s, out vt))
                        {
                            result.Add(key, vt);
                        }
                        else
                        {
                            result.Add(key, s);
                        }
                    }
                    else
                    {
                        var d = dictionary[key] as IDictionary<string, object>;
                        var a = dictionary[key] as List<object>;
                        if (d != null)
                        {
                            result.Add(key, ParseVariables(d));
                        }
                        else if (a != null)
                        {
                            result.Add(key, ParseVariables(a));
                        }
                        else
                        {
                            result.Add(key, dictionary[key]);
                        }
                    }
                }
            }
            return result;
        }

        private static List<object> ParseVariables(List<object> array)
        {
            var result = new List<object>();
            if (array != null)
            {
                foreach (var element in array)
                {
                    var s = element as string;
                    if (s != null)
                    {
                        VariableToken vt;
                        if (VariableToken.TryParse(s, out vt))
                        {
                            result.Add(vt);
                        }
                        else
                        {
                            result.Add(s);
                        }
                    }
                    else
                    {
                        var d = element as IDictionary<string, object>;
                        var a = element as List<object>;
                        if (d != null)
                        {
                            result.Add(ParseVariables(d));
                        }
                        else if (a != null)
                        {
                            result.Add(ParseVariables(a));
                        }
                        else
                        {
                            result.Add(element);
                        }
                    }
                }
            }
            return result;
        }

        private static IDictionary<string, object> EvaluateVariables(IDictionary<string, object> dictionary, Execution.ExecutionContext context)
        {
            var result = new Dictionary<string, object>();
            if (dictionary != null)
            {
                foreach (var key in dictionary.Keys)
                {
                    var vt = dictionary[key] as VariableToken;
                    if (vt != null)
                    {
                        result.Add(key, vt.Evaluate(context));
                    }
                    else
                    {
                        var d = dictionary[key] as IDictionary<string, object>;
                        var a = dictionary[key] as List<object>;
                        if (d != null)
                        {
                            result.Add(key, EvaluateVariables(d, context));
                        }
                        else if (a != null)
                        {
                            result.Add(key, EvaluateVariables(a, context));
                        }
                        else
                        {
                            result.Add(key, dictionary[key]);
                        }
                    }
                }
            }
            return result;
        }

        private static List<object> EvaluateVariables(List<object> array, Execution.ExecutionContext context)
        {
            var result = new List<object>();
            if (array != null)
            {
                foreach (var element in array)
                {
                    var vt = element as VariableToken;
                    if (vt != null)
                    {
                        result.Add(vt.Evaluate(context));
                    }
                    else
                    {
                        var d = element as IDictionary<string, object>;
                        var a = element as List<object>;
                        if (d != null)
                        {
                            result.Add(EvaluateVariables(d, context));
                        }
                        else if (a != null)
                        {
                            result.Add(EvaluateVariables(a, context));
                        }
                        else
                        {
                            result.Add(element);
                        }
                    }
                }
            }
            return result;
        }

        private static IDictionary<string, object> HideVariables(IDictionary<string, object> dictionary)
        {
            var result = new Dictionary<string, object>();
            if (dictionary != null)
            {
                foreach (var key in dictionary.Keys)
                {
                    var vt = dictionary[key] as VariableToken;
                    if (vt != null)
                    {
                        result.Add(key, vt.ToString());
                    }
                    else
                    {
                        var d = dictionary[key] as IDictionary<string, object>;
                        var a = dictionary[key] as List<object>;
                        if (d != null)
                        {
                            result.Add(key, HideVariables(d));
                        }
                        else if (a != null)
                        {
                            result.Add(key, HideVariables(a));
                        }
                        else
                        {
                            result.Add(key, dictionary[key]);
                        }
                    }
                }
            }
            return result;
        }

        private static List<object> HideVariables(List<object> array)
        {
            var result = new List<object>();
            if (array != null)
            {
                foreach (var element in array)
                {
                    var vt = element as VariableToken;
                    if (vt != null)
                    {
                        result.Add(vt.ToString());
                    }
                    else
                    {
                        var d = element as IDictionary<string, object>;
                        var a = element as List<object>;
                        if (d != null)
                        {
                            result.Add(HideVariables(d));
                        }
                        else if (a != null)
                        {
                            result.Add(HideVariables(a));
                        }
                        else
                        {
                            result.Add(element);
                        }
                    }
                }
            }
            return result;
        }
    }
}
