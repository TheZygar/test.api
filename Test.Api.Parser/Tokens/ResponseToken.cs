﻿using System.Collections.Generic;
using System.Linq;

namespace Test.Api.Parser.Tokens
{
    public class ResponseToken : Token
    {
        public IDictionary<string, object> Response { get; set; }

        public ResponseToken(IDictionary<string, object> response)
        {
            if (response != null)
            {
                Response = response;
            }
            else
            {
                Response = new Dictionary<string, object>();
            }
        }

        public override string ToString()
        {
            return ResponseIdentifier + GetFormattedJsonString(Response);
        }

        public override object Evaluate(Execution.ExecutionContext context)
        {
            return GetFormattedJsonString(Response);
        }

        public static bool TryParse(string statement, out ResponseToken token)
        {
            statement = statement.Trim().TrimEnd(EndOfStatement).Trim();

            if (statement.First() == ResponseIdentifier)
            {
                statement = statement.TrimStart(ResponseIdentifier);
                try
                {
                    token = new ResponseToken(GetDictionary(statement));
                    return true;
                }
                catch { }
            }

            token = null;
            return false;
        }
    }
}
