﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Test.Api.Parser.Collections;
using Test.Api.Parser.Exceptions;

namespace Test.Api.Parser.Tokens
{
    public abstract class Token
    {
        public enum Method
        {
            Get,
            Post
        }

        public virtual void WriteTo(System.IO.TextWriter writer, Masks.Mask[] masks)
        {
            if (masks != null)
            {
                foreach (var mask in masks)
                {
                    mask.ApplyMask(this);
                }
            }
            writer.Write(this.ToString());
            if (!(this is CommentToken))
            {
                writer.Write(EndOfStatement + EndOfLineForWrite);
            }
        }

        public abstract override string ToString();

        public abstract object Evaluate(Execution.ExecutionContext context);

        public const char EndOfStatement = ';';
        public const char ResponseIdentifier = '$';
        public const char DirectiveIdentifier = '#';
        public const string CommentIdentifier = "//";
        public const string EndOfLineForWrite = "\r\n";
        public const char EndOfLineForRead = '\n';
        public const char StringIdentifier = '\"';
        public const char StringEscapeIdentifier = '\\';
        public const char VariableStartIdentifier = '{';
        public const char VariableEndIdentifier = '}';
        public const char MethodStartIdentifier = '(';
        public const char MethodEndIdentifier = ')';
        public const char AssignmentIdentifier = '=';
        public const char Comma = ',';

        public const string RequestGetIdentifier = "GET ";
        public const string RequestPostIdentifier = "POST ";
        public const char RequestQueryIdentifier = '?';
        public const char RequestQueryAlsoIdentifier = '&';

        protected static IEnumerable<string> SplitCommaDelimited(string statement)
        {
            StringBuilder result = new StringBuilder();
            bool isInQuote = false;
            char? previousCharacter = null;
            foreach (var character in statement)
            {
                if (character == Token.StringIdentifier)
                {
                    if (previousCharacter != Token.StringEscapeIdentifier)
                    {
                        isInQuote = !isInQuote;
                    }
                }
                if (!isInQuote && character == Token.Comma)
                {
                    yield return result.ToString();
                    result.Clear();
                }
                else
                {
                    result.Append(character);
                }
                previousCharacter = character;
            }
            yield return result.ToString();
        }

        protected static bool IsRequestToken(string statement)
        {
            string request = statement.Trim();
            if (request.StartsWith(RequestGetIdentifier) || request.StartsWith(RequestPostIdentifier))
            {
                return true;
            }
            return false;
        }

        protected static bool IsDirectiveToken(string statement)
        {
            string request = statement.Trim();
            if (request.StartsWith(DirectiveIdentifier.ToString()))
            {
                return true;
            }
            return false;
        }

        protected static bool IsResponseToken(string statement)
        {
            string request = statement.Trim();
            if (request.StartsWith(ResponseIdentifier.ToString()))
            {
                return true;
            }
            return false;
        }

        protected static bool IsAssignmentToken(string statement)
        {
            string request = statement.Trim();
            var parts = statement.Split(new char[] { AssignmentIdentifier }, 2);
            if (parts.Count() != 2)
            {
                return false;
            }
            var assignee = parts[0].Trim();
            if (IsSimpleIdentifier(assignee))
            {
                return true;
            }
            return false;
        }

        protected static bool IsSimpleIdentifier(string statement)
        {
            if (!string.IsNullOrEmpty(statement) && char.IsLetter(statement.First()) &&
                statement.Count(x => !(char.IsLetterOrDigit(x) || x == '_' || x == '.')) == 0)
            {
                return true;
            }
            return false;
        }

        protected static bool IsDictionaryLookup(string statement)
        {
            if (!string.IsNullOrEmpty(statement) && char.IsLetter(statement.First()) &&
                statement.Count(x => !(char.IsLetterOrDigit(x) || x == '_' || x == '.' || x == '"' || x == '[' || x == ']')) == 0)
            {
                return true;
            }
            return false;
        }

        protected static IDictionary<string, object> GetDictionary(string statement)
        {
            try
            {
                var jsonSerializer = JsonSerializer.Create(Execution.ExecutionContext.Settings);

                return JsonHandler.Deserialize(statement) as IDictionary<string, object>;
            }
            catch { }
            return null;
        }

        protected static string GetJsonString(IDictionary<string, object> dictionary)
        {
            if (dictionary == null)
                return null;

            try
            {
                var jsonSerializer = JsonSerializer.Create(Execution.ExecutionContext.Settings);
                using (StringWriter writer = new StringWriter())
                {
                    jsonSerializer.Serialize(writer, dictionary);
                    return writer.ToString();
                }
            }
            catch { }
            return null;
        }

        protected static string GetFormattedJsonString(IDictionary<string, object> dictionary)
        {
            if (dictionary == null)
                return null;

            try
            {
                var jsonSerializer = JsonSerializer.Create(Execution.ExecutionContext.Settings);
                var json = Newtonsoft.Json.Linq.JObject.FromObject(dictionary, jsonSerializer);
                return json.ToString();
            }
            catch { }
            return null;
        }

        public static bool TryParse(string statement, int line, out Token token, ref List<ParserException> exceptions)
        {
            try
            {
                if (IsRequestToken(statement))
                {
                    RequestToken t;
                    if (RequestToken.TryParse(statement, out t))
                    {
                        token = t;
                        return true;
                    }
                }
                else if (IsResponseToken(statement))
                {
                    ResponseToken t;
                    if (ResponseToken.TryParse(statement, out t))
                    {
                        token = t;
                        return true;
                    }
                }
                else if (IsAssignmentToken(statement))
                {
                    AssignmentToken t;
                    if (AssignmentToken.TryParse(statement, out t))
                    {
                        token = t;
                        return true;
                    }
                }
                else if (IsDirectiveToken(statement))
                {
                    DirectiveToken t;
                    if (DirectiveToken.TryParse(statement, out t))
                    {
                        token = t;
                        return true;
                    }
                }
                else
                {
                    MethodToken t;
                    if (MethodToken.TryParse(statement, out t))
                    {
                        token = t;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                exceptions.Add(new ParserException(string.Format("{0} at line {1}", ex.Message, line), ex));
            }

            token = null;
            return false;
        }
    }
}
