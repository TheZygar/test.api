﻿using System.Collections.Generic;
using Test.Api.Parser.Tokens;

namespace Test.Api.Parser.Masks
{
    public class ValueMask : Mask
    {
        public ValueMask(string value, string replacement)
        {
            Value = value;
            Replacement = replacement;
        }

        public string Value { get; private set; }

        public string Replacement { get; private set; }

        public override Token ApplyMask(Token token)
        {
            var responseToken = token as ResponseToken;
            if (responseToken != null)
            {
                responseToken.Response = ApplyMask(responseToken.Response);
                return responseToken;
            }

            return token;
        }

        private IDictionary<string, object> ApplyMask(IDictionary<string, object> dictionary)
        {
            var result = new Dictionary<string, object>();
            foreach (var key in dictionary.Keys)
            {
                if ((dictionary[key] as string) == Value)
                {
                    result.Add(key, Replacement);
                }
                else
                {
                    var sub = dictionary[key] as IDictionary<string, object>;
                    if (sub != null)
                    {
                        result.Add(key, ApplyMask(sub));
                    }
                    else
                    {
                        var array = dictionary[key] as List<object>;
                        if (array != null)
                        {
                            result.Add(key, ApplyMask(array));
                        }
                        else
                        {
                            result.Add(key, dictionary[key]);
                        }
                    }
                }
            }
            return result;
        }

        private List<object> ApplyMask(List<object> array)
        {
            var result = new List<object>();
            foreach (var o in array)
            {
                var sub = o as IDictionary<string, object>;
                if (sub != null)
                {
                    result.Add(ApplyMask(sub));
                }
                else
                {
                    var a = o as List<object>;
                    if (a != null)
                    {
                        result.Add(ApplyMask(a));
                    }
                    else if ((o as string) == Value)
                    {
                        result.Add(Replacement);
                    }
                    else
                    {
                        result.Add(o);
                    }
                }
            }
            return result;
        }
    }
}
