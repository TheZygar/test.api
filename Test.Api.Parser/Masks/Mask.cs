﻿using Test.Api.Parser.Tokens;

namespace Test.Api.Parser.Masks
{
    public abstract class Mask
    {
        public virtual Token ApplyMask(Token token)
        {
            return token;
        }
    }
}
