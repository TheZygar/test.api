﻿using System.IO;
using Test.Api.Parser.Masks;
using Test.Api.Parser.Tokens;

namespace Test.Api.Parser
{
    public class TokenManager
    {
        public TokenManager()
        {
        }

        public string Save(string destination, Token[] tokens, Mask[] masks)
        {
            var utf8WithoutBom = new System.Text.UTF8Encoding(false);

            using (var writer = new StreamWriter(destination, false, utf8WithoutBom))
            {
                bool previousWasComment = true; //In case there is a comment at the beginning of the file
                foreach (var token in tokens)
                {
                    bool currentIsComment = token is CommentToken;
                    if (!previousWasComment && currentIsComment)
                    {
                        writer.Write(Token.EndOfLineForWrite);
                    }

                    token.WriteTo(writer, masks);

                    previousWasComment = currentIsComment;
                }
            }
            return destination;
        }
    }
}
