﻿using System;
using System.Runtime.Serialization;

namespace Test.Api.Parser.Exceptions
{
    [Serializable]
    public class ParserFileException : Exception
    {
        public ParserFileException(string message, string path)
            : base(message)
        {
            Path = path;
        }

        public string Path { get; protected set; }

        public override string Message
        {
            get
            {
                return "Unable to Parse file at location: " + Path + "\r\n" + base.Message;
            }
        }

        protected ParserFileException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Path = info.GetString("Path");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Path", Path);
            base.GetObjectData(info, context);
        }
    }
}
