﻿using System;
using System.Runtime.Serialization;

namespace Test.Api.Parser.Exceptions
{
    [Serializable]
    public class ResponseException : Exception
    {
        public ResponseException(string message)
            : base(message)
        {
        }

        public ResponseException(string message, Exception exception)
            : base(message, exception)
        {
        }

        protected ResponseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
