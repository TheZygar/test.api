﻿using System;
using System.Runtime.Serialization;

namespace Test.Api.Parser.Exceptions
{
    [Serializable]
    public class EvaluationException : Exception
    {
        public EvaluationException(string message)
            : base(message)
        {
        }

        public EvaluationException(string message, Exception exception)
            : base(message, exception)
        {
        }

        protected EvaluationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
