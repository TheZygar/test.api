﻿using System;

namespace Test.Api.Parser.Execution
{
    [AttributeUsage(AttributeTargets.Method)]
    public class CommandAttribute : Attribute
    {
    }
}
