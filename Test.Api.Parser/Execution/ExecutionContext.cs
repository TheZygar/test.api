﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using Test.Api.Parser.Masks;

namespace Test.Api.Parser.Execution
{
    public class ExecutionContext : IDisposable
    {
        private static JsonSerializerSettings _settings = null;

        public static JsonSerializerSettings Settings
        {
            get
            {
                if (_settings == null)
                {
                    _settings = new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Error,
                        DateParseHandling = DateParseHandling.DateTimeOffset,
                        DateFormatHandling = DateFormatHandling.IsoDateFormat,
                        DateFormatString = "yyyy-MM-ddTHH:mm:sszzz",
                        DateTimeZoneHandling = DateTimeZoneHandling.Local,
                        ConstructorHandling = ConstructorHandling.Default,
                    };
                }
                return _settings;
            }
        }

        // Default the timeout to 10 minutes
        public const int DefaultTimeout = 600000;

        public ExecutionContext(ICommandEngine commandEngine)
        {
            _variables = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
            _commandEngine = commandEngine;
            _masks = new List<Mask>();

            _webClient = new CookieAwareWebClient();
        }

        private IDictionary<string, object> _variables = null;

        public IDictionary<string, object> Variables
        {
            get
            {
                return _variables;
            }
        }

        private ICommandEngine _commandEngine = null;

        public ICommandEngine CommandEngine
        {
            get
            {
                return _commandEngine;
            }
        }

        private List<Mask> _masks = null;

        public List<Mask> Masks
        {
            get
            {
                return _masks;
            }
        }

        private WebClient _webClient = null;

        public WebClient WebClient
        {
            get
            {
                return _webClient;
            }
        }

        public string CurrentTestLocation { get; set; }
        public string TestFileRoot { get; set; }

        /// <summary>
        /// Returns true if the key was updated, false if added
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool AddOrUpdate(string key, object value)
        {
            if (Variables.ContainsKey(key))
            {
                Variables[key] = value;
                return true;
            }
            else
            {
                Variables.Add(key, value);
                return false;
            }
        }

        public object ListLookup(string accessor, List<object> context)
        {
            try
            {
                int next = accessor.IndexOfAny(new char[] { '.', '[' });
                if (next >= 0)
                {
                    string currentIdentifier = accessor.Substring(0, next);
                    if (accessor[next] == '[')
                    {
                        string lookup = accessor.Substring(next + 1, accessor.IndexOf(']') - (next + 1));
                        int index;
                        if (int.TryParse(lookup, out index))
                        {
                            var nextIdentifier = accessor.Substring(accessor.IndexOf(']') + 1);
                            if (string.IsNullOrEmpty(nextIdentifier))
                            {
                                return context[index];
                            }
                            else
                            {
                                return DictionaryLookup(nextIdentifier, context[index] as IDictionary<string, object>);
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else //Assume period character
                    {
                        return null;
                    }
                }
                else
                {
                    return context;
                }
            }
            catch
            {
                return null;
            }
        }

        public object DictionaryLookup(string accessor, IDictionary<string, object> context)
        {
            try
            {
                int next = accessor.IndexOfAny(new char[] { '.', '[' });
                if (next >= 0)
                {
                    string currentIdentifier = accessor.Substring(0, next);
                    if (accessor[next] == '[')
                    {
                        string lookup = accessor.Substring(next + 1, accessor.IndexOf(']') - (next + 1));
                        int index;
                        if (int.TryParse(lookup, out index))
                        {
                            var local = context[currentIdentifier] as List<object>;
                            var nextIdentifier = accessor.Substring(accessor.IndexOf(']') + 1);
                            if (string.IsNullOrEmpty(nextIdentifier))
                            {
                                return local[index];
                            }
                            else
                            {
                                if (local[index] is IDictionary<string, object>)
                                {
                                    return DictionaryLookup(nextIdentifier, local[index] as IDictionary<string, object>);
                                }
                                else
                                {
                                    return ListLookup(nextIdentifier, local[index] as List<object>);
                                }
                            }
                        }
                        else
                        {
                            var local = context[currentIdentifier] as IDictionary<string, object>;
                            var nextIdentifier = accessor.Substring(accessor.IndexOf(']') + 1);
                            lookup = lookup.Trim('"');
                            if (string.IsNullOrEmpty(nextIdentifier))
                            {
                                return local[lookup];
                            }
                            else
                            {
                                if (local[lookup] is IDictionary<string, object>)
                                {
                                    return DictionaryLookup(nextIdentifier, local[lookup] as IDictionary<string, object>);
                                }
                                else
                                {
                                    return ListLookup(nextIdentifier, local[lookup] as List<object>);
                                }
                            }
                        }
                    }
                    else //Assume period character
                    {
                        int after = accessor.IndexOfAny(new char[] { '.', '[' }, next + 1);
                        if (after < 0)
                        {
                            after = accessor.Length;
                        }

                        if (string.IsNullOrEmpty(currentIdentifier))
                        {
                            string lookup = accessor.Substring(next + 1, after - (next + 1));
                            var nextIdentifier = accessor.Substring(after);
                            if (string.IsNullOrEmpty(nextIdentifier))
                            {
                                return context[lookup];
                            }
                            else
                            {
                                if (context[lookup] is IDictionary<string, object>)
                                {
                                    return DictionaryLookup(nextIdentifier, context[lookup] as IDictionary<string, object>);
                                }
                                else
                                {
                                    return ListLookup(nextIdentifier, context[lookup] as List<object>);
                                }
                            }
                        }
                        else
                        {
                            var local = context[currentIdentifier] as IDictionary<string, object>;
                            string lookup = accessor.Substring(next + 1, after - (next + 1));
                            var nextIdentifier = accessor.Substring(after);
                            if (string.IsNullOrEmpty(nextIdentifier))
                            {
                                return local[lookup];
                            }
                            else
                            {
                                if (local[lookup] is IDictionary<string, object>)
                                {
                                    return DictionaryLookup(nextIdentifier, local[lookup] as IDictionary<string, object>);
                                }
                                else
                                {
                                    return ListLookup(nextIdentifier, local[lookup] as List<object>);
                                }
                            }
                        }
                    }
                }
                else
                {
                    return context[accessor];
                }
            }
            catch
            {
                return null;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_webClient != null)
                {
                    _webClient.Dispose();
                    _webClient = null;
                }
            }
        }

        ~ExecutionContext()
        {
            Dispose(false);
        }
    }
}
