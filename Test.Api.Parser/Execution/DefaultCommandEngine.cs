﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Test.Api.Parser.Execution
{
    public partial class DefaultCommandEngine : ICommandEngine
    {
        private ExecutionContext _context;

        public object Execute(ExecutionContext context, string command, object[] parameters)
        {
            try
            {
                _context = context;
                var method = GetType().GetMethod(command);
                var p = method.GetParameters();
                if (p.Length != parameters.Length)
                {
                    if (p.Length == 1 && Attribute.IsDefined(p.First(), typeof(ParamArrayAttribute)))
                    {
                        return GetType().GetMethod(command).Invoke(this, new object[] { parameters });
                    }
                }

                return GetType().GetMethod(command).Invoke(this, parameters);
            }
            catch (TargetInvocationException ex)
            {
                throw new Parser.Exceptions.EvaluationException(string.Format("Error calling command {0}. {1}", command, ex.InnerException.Message), ex.InnerException);
            }
            catch (Exception ex)
            {
                throw new Parser.Exceptions.EvaluationException(string.Format("Error calling command {0}. {1}", command, ex.Message), ex);
            }
        }

        public string[] GetAvailableMethods()
        {
            return GetType().GetMethods().Where(y => y.GetCustomAttributes().OfType<CommandAttribute>().Any()).Select(x => x.Name).ToArray();
        }

        [Command]
        public void ClearMasks()
        {
            _context.Masks.Clear();
        }

        [Command]
        public void AddFieldMask(string field, string replacement)
        {
            _context.Masks.Add(new Parser.Masks.FieldMask(field, replacement));
        }

        [Command]
        public void AddValueMask(string value, string replacement)
        {
            _context.Masks.Add(new Parser.Masks.ValueMask(value, replacement));
        }

        [Command]
        public void AddIgnoreMask(string field)
        {
            _context.Masks.Add(new Parser.Masks.IgnoreMask(field));
        }

        [Command]
        public bool WaitForResult(string uri, string method, string expected, System.Numerics.BigInteger timeout)
        {
            var webClient = _context.WebClient;

            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();
            while (true)
            {
                if (watch.ElapsedMilliseconds > timeout)
                {
                    throw new Exception(string.Format("Deadlocked on {0}", method));
                }
                try
                {
                    var result = Encoding.UTF8.GetString(webClient.DownloadData(uri + method));
                    if (result == expected)
                    {
                        return true;
                    }
                }
                catch
                {
                    Thread.Sleep(1);
                }
            }
        }

        [Command]
        public void AddTypeMask(string type, string replacement)
        {
            var parsedType = Type.GetType(type);
            _context.Masks.Add(new Parser.Masks.TypeMask(parsedType, replacement));
            if (parsedType == typeof(DateTime))
            {
                _context.Masks.Add(new Parser.Masks.TypeMask(typeof(DateTimeOffset), replacement));
            }
        }

        [Command]
        public string Concat(params object[] strings)
        {
            return string.Concat(strings.Cast<string>().ToArray());
        }

        [Command]
        public string UrlEncode(string value)
        {
            return System.Web.HttpUtility.UrlEncode(value);
        }

        [Command]
        public string JsonEncode(string value)
        {
            return value.Replace("\"", "\\\"");
        }

        [Command]
        public void Wait(System.Numerics.BigInteger duration)
        {
            Thread.Sleep((int)duration);
        }

        [Command]
        public string Rtf2Html(string value)
        {
            var html = HtmlFromXamlConverter.ConvertXamlToHtml(ConvertRtfToXaml(value));

            html = Regex.Replace(html, "<html[^><]*>|<.html[^><]*>", "", RegexOptions.IgnoreCase);
            html = Regex.Replace(html, "<body[^><]*>|<.body[^><]*>", "", RegexOptions.IgnoreCase);

            return html;
        }

        [Command]
        public string DateAdd(string date, System.Numerics.BigInteger days)
        {
            DateTime d;
            if (!DateTime.TryParse(date, out d))
            {
                d = DateTime.Today;
            }
            d = d.AddDays((int)days);
            return d.ToString("yyyy-MM-dd");
        }

        [Command]
        public string DateTimeAddHours(string dateTime, object hours)
        {
            decimal offset = 0;
            if (hours is decimal)
            {
                offset = (decimal)hours;
            }
            else
            {
                try
                {
                    offset = (decimal)(System.Numerics.BigInteger)hours;
                }
                catch { }
            }

            DateTimeOffset d;
            if (!DateTimeOffset.TryParse(dateTime, out d))
            {
                d = DateTimeOffset.UtcNow;
            }
            d = d.AddHours((double)offset);
            return d.ToString("yyyy-MM-ddTHH:mm:sszzz");
        }

        [Command]
        public bool AssertAreEqual(object a, object b)
        {
            if (a == b)
            {
                return true;
            }

            DateTimeOffset da;
            if (DateTimeOffset.TryParse(a.ToString(), out da))
            {
                DateTimeOffset db;
                if (DateTimeOffset.TryParse(b.ToString(), out db))
                {
                    if (da == db)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        [Command]
        public bool AssertAreNotEqual(object a, object b)
        {
            return !AssertAreEqual(a, b);
        }

        [Command]
        public virtual string ReadFtpString(string url, string user, string password, string type)
        {
            var ftpRequest = (FtpWebRequest)WebRequest.Create(url);
            ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
            ftpRequest.UseBinary = true;
            ftpRequest.EnableSsl = true;

            ftpRequest.Credentials = new NetworkCredential(user, password);

            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            using (var ftpResponse = (FtpWebResponse)ftpRequest.GetResponse())
            {
                using (var ftpStream = ftpResponse.GetResponseStream())
                {
                    if (type.ToLowerInvariant() == "binary")
                    {
                        byte[] bytes = null;
                        var memoryStream = new MemoryStream(1024);
                        ftpStream.CopyTo(memoryStream);
                        bytes = memoryStream.ToArray();
                        return Convert.ToBase64String(bytes.ToArray());
                    }
                    else
                    {
                        var ftpReader = new StreamReader(ftpStream, UTF8Encoding.UTF8);
                        return ftpReader.ReadToEnd();
                    }
                }
            }
        }

        [Command]
        public virtual string ReadFileString(string path, string type)
        {
            string rooted = path;
            if (!Path.IsPathRooted(rooted))
            {
                rooted = Path.Combine(_context.TestFileRoot, rooted);
            }
            if (!File.Exists(rooted))
            {
                rooted = Path.Combine(_context.CurrentTestLocation, rooted);
            }
            if (File.Exists(rooted))
            {
                if (type.ToLowerInvariant() == "binary")
                {
                    byte[] bytes = File.ReadAllBytes(rooted);
                    return Convert.ToBase64String(bytes.ToArray());
                }
                else
                {
                    return File.ReadAllText(rooted, UTF8Encoding.UTF8);
                }
            }
            else
            {
                throw new FileNotFoundException("Could not find file", rooted);
            }
        }
    }
}
