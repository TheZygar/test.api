﻿namespace Test.Api.Parser.Execution
{
    public interface ICommandEngine
    {
        object Execute(ExecutionContext context, string command, object[] parameters);

        string[] GetAvailableMethods();
    }
}
