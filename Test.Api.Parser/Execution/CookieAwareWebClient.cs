﻿using System;
using System.Net;

namespace Test.Api.Parser.Execution
{
    public class CookieAwareWebClient : WebClient
    {
        public CookieAwareWebClient()
            : base()
        {
            Encoding = System.Text.Encoding.UTF8;
        }

        private readonly CookieContainer m_container = new CookieContainer();

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls;
            HttpWebRequest webRequest = request as HttpWebRequest;
            if (webRequest != null)
            {
                webRequest.CookieContainer = m_container;
                var assemblyName = System.Reflection.Assembly.GetEntryAssembly().GetName();
                webRequest.UserAgent = "Zygar/" + assemblyName.Version.ToString() + " (" + assemblyName.Name + ")";
                webRequest.ContentType = "application/json";
            }
            return request;
        }
    }
}
