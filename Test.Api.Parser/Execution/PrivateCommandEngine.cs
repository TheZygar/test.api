﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace Test.Api.Parser.Execution
{
    public partial class DefaultCommandEngine
    {
        private static Task<T> StartSTATask<T>(Func<T> func)
        {
            var tcs = new TaskCompletionSource<T>();
            Thread thread = new Thread(() =>
            {
                try
                {
                    tcs.SetResult(func());
                }
                catch (Exception e)
                {
                    tcs.SetException(e);
                }
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            return tcs.Task;
        }

        private string ConvertRtfToXaml(string rtfText)
        {
            var task = StartSTATask(() =>
            {
                var richTextBox = new System.Windows.Controls.RichTextBox();
                if (string.IsNullOrEmpty(rtfText))
                    return "";
                var textRange = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);
                using (var rtfMemoryStream = new MemoryStream())
                {
                    var rtfStreamWriter = new StreamWriter(rtfMemoryStream);
                    rtfStreamWriter.Write(rtfText);
                    rtfStreamWriter.Flush();
                    rtfMemoryStream.Seek(0, SeekOrigin.Begin);
                    textRange.Load(rtfMemoryStream, System.Windows.DataFormats.Rtf);
                }
                using (var rtfMemoryStream = new MemoryStream())
                {
                    textRange = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);
                    textRange.Save(rtfMemoryStream, System.Windows.DataFormats.Xaml);
                    rtfMemoryStream.Seek(0, SeekOrigin.Begin);
                    var rtfStreamReader = new StreamReader(rtfMemoryStream);
                    return rtfStreamReader.ReadToEnd();
                }
            });
            return task.Result;
        }
    }
}
