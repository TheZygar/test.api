﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Test.Api.Parser.Exceptions;
using Test.Api.Parser.Tokens;

namespace Test.Api.Parser
{
    public class ParseManager
    {
        public ParseManager()
        {
        }

        private List<ParserException> _exceptions = new List<ParserException>();

        public List<ParserException> Exceptions
        {
            get
            {
                return _exceptions;
            }
        }

        public IEnumerable<Token> ParseFile(string path)
        {
            if (!File.Exists(path))
            {
                throw new ParserFileException("Unable to find file for parsing.", path);
            }

            string extension = Path.GetExtension(path).ToLowerInvariant();
            if (extension == ".test" || extension == ".result")
            {
                using (var file = new StreamReader(path, Encoding.UTF8))
                {
                    int lineNumber = 1;
                    int linePosition = 1;

                    int statementLineNumber = lineNumber;
                    int statementLinePosition = linePosition;

                    StringBuilder statement = new StringBuilder();

                    bool endStatement = false;
                    bool isInQuote = false;
                    char? previousCharacter = null;
                    int value = file.Read();
                    while (value >= 0)
                    {
                        CommentToken commentToken;
                        Token token;
                        char character = (char)value;

                        statement.Append(character);
                        if (statement.ToString().Trim().StartsWith(Token.CommentIdentifier) && character == Token.EndOfLineForRead)
                        {
                            if (CommentToken.TryParse(statement.ToString(), out commentToken))
                            {
                                yield return commentToken;
                                endStatement = true;
                            }
                            else
                            {
                                Exceptions.Add(new ParserException(string.Format("Unable to parse Comment at line {0} \r\n{1}", lineNumber, statement)));
                            }
                        }
                        else if (character == Token.StringIdentifier)
                        {
                            if (previousCharacter != Token.StringEscapeIdentifier)
                            {
                                isInQuote = !isInQuote;
                            }
                        }
                        else if (!isInQuote && character == Token.EndOfStatement && Token.TryParse(statement.ToString(), lineNumber, out token, ref _exceptions))
                        {
                            yield return token;
                            endStatement = true;
                        }

                        if (character == Token.EndOfLineForRead)
                        {
                            lineNumber++;
                        }
                        else
                        {
                            linePosition++;
                        }
                        if (endStatement)
                        {
                            statement.Clear();
                            statementLineNumber = lineNumber;
                            statementLinePosition = linePosition;
                            endStatement = false;
                            isInQuote = false;
                            previousCharacter = null;
                        }

                        value = file.Read();
                        previousCharacter = character;
                    }
                    if (statement.ToString().Trim().Length > 0)
                    {
                        throw new EndOfStreamException(string.Format("Parser found unexpected end of file at line {0} position {1}. Expected at line {2} position {3}. \r\n{4}", lineNumber, linePosition, statementLineNumber, statementLinePosition, statement));
                    }
                }
            }
            else
            {
                throw new ParserFileException("Invalid extension", path);
            }
        }
    }
}
